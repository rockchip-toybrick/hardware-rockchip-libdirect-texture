/*
	Create By randall.zhuo@rock-chips.com
	2018/11/01

*/
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sched.h>
#include <sys/resource.h>

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <ui/GraphicBuffer.h>
//#include <EGLUtils.h>

#include <list>

#include "direct_texture.h"

using namespace android;

static const int texUsage = GraphicBuffer::USAGE_HW_TEXTURE | GraphicBuffer::USAGE_SW_WRITE_RARELY;

typedef struct _DirectTexture {
	GLuint texId;
	sp<GraphicBuffer> texBuffer;
	EGLClientBuffer clientBuffer;
	EGLImageKHR img;
	bool locked;
	char *pixels; // GraphicBuffer
	char *data;
	int texWidth;
	int texHeight;
	int format;
	int bytePerPixel;
	int stride;
}DirectTexture;

static std::list<DirectTexture *> dtList;

static int glColorFmtToHalFmt(int fmt) {

	switch(fmt) {
		case GL_RGB:
			return HAL_PIXEL_FORMAT_RGB_888;
		case GL_RGBA:
			return HAL_PIXEL_FORMAT_RGBA_8888;
		default:
			ALOGD("Unsupport fmt:%d\n", fmt);
			break;
	}
	
	return -1;
}

static int getBytePerPixel(int fmt) {

	switch(fmt) {
		case GL_RGB:
			return 3;
		case GL_RGBA:
			return 4;
		default:
			ALOGD("Unsupport fmt:%d\n", fmt);
			break;
	}
	
	return 0;
}

static DirectTexture * getDirectTexture(int texId) {

	std::list<DirectTexture *>::iterator iter;

	for(iter = dtList.begin(); iter != dtList.end(); iter++)
	{
		DirectTexture *dt =  (DirectTexture *)*iter;

		if ((dt == NULL) || (dt->texId != (GLuint)texId)) {
			continue;
		}

		return dt;
	}

	return NULL;
}

int createDirectTexture(int texWidth, int texHeight, int format) {

	int texFormat = glColorFmtToHalFmt(format);

	if (texFormat == -1) {
		return -1;
	}

	EGLDisplay dpy = eglGetDisplay(EGL_DEFAULT_DISPLAY);

	if (dpy == EGL_NO_DISPLAY) {
        ALOGD("eglGetDisplay returned EGL_NO_DISPLAY.\n");
        return -2;
    }

	DirectTexture *dt = new DirectTexture();

	dt->texId = -1;
	dt->locked = false;
	dt->pixels = NULL;
	dt->data = NULL;
	dt->texWidth = texWidth;
	dt->texHeight = texHeight;
	dt->format = format;
	dt->bytePerPixel = getBytePerPixel(format);
	dt->img = EGL_NO_IMAGE_KHR;


	dt->texBuffer = new GraphicBuffer(texWidth, texHeight, texFormat, texUsage);

	dt->stride = dt->texBuffer->getStride();

	if (dt->stride != texWidth) {
		dt->data = (char *)malloc(texWidth * texHeight * dt->bytePerPixel);
	}

	dt->clientBuffer = (EGLClientBuffer)dt->texBuffer->getNativeBuffer();

	dt->img = eglCreateImageKHR(dpy, EGL_NO_CONTEXT, EGL_NATIVE_BUFFER_ANDROID, dt->clientBuffer, 0);

	if (dt->img == EGL_NO_IMAGE_KHR) {
		goto error;
	}

	glGenTextures(1, &dt->texId);
	//glBindTexture(GL_TEXTURE_EXTERNAL_OES, dt->texId);
	glBindTexture(GL_TEXTURE_2D, dt->texId);
	glEGLImageTargetTexture2DOES(GL_TEXTURE_2D, (GLeglImageOES)dt->img);

	dtList.push_back(dt);

	return (int)(dt->texId);
error:

	if (dt) {

		if (dt->texId != (GLuint)-1) {
			glDeleteTextures(1, &dt->texId);	
		}

		delete dt;
		dt = nullptr;
	}

	return -3;
}

bool deleteDirectTexture(int texId) {


	std::list<DirectTexture *>::iterator iter;

	for(iter = dtList.begin(); iter != dtList.end(); iter++)
	{
		DirectTexture *dt =  (DirectTexture *)*iter;

		if ((dt == nullptr) || (dt->texId != (GLuint)texId)) {
			continue;
		}

		if (dt->locked) {
			dt->texBuffer->unlock();

			dt->pixels = nullptr;
			dt->locked = false;
		}

		if (dt->data != nullptr) {
			free(dt->data);
		}


		if (dt->texId != (GLuint)-1) {
			glDeleteTextures(1, &dt->texId);
		}

		if (dt->img != EGL_NO_IMAGE_KHR) {
			eglDestroyImageKHR(eglGetDisplay(EGL_DEFAULT_DISPLAY), dt->img);
		}

		dt->texBuffer = nullptr;
		delete dt;

		dtList.erase(iter);  
		break;
	}


	return true;
}

char* requireBufferByTexId(int texId) {
	DirectTexture *dt =  getDirectTexture(texId);

	if (dt == nullptr) {
		ALOGE("Invalid texture id:%d\n", texId);
		return nullptr;
	}

	if (dt->locked) {
		if (dt->texWidth != dt->stride) {
			return dt->data;
		}

		return dt->pixels;
	}

	status_t err = dt->texBuffer->lock(GRALLOC_USAGE_SW_WRITE_OFTEN, (void**)(&dt->pixels));
	if (err != 0) {
		ALOGE("Get buffer failed: %d\n", err);
		return nullptr;
	}

	dt->locked = true;

	if (dt->texWidth != dt->stride) {
		for (int i=0; i< dt->texHeight; ++i) {
			memcpy(dt->data + i * dt->texWidth * dt->bytePerPixel, dt->pixels + i * dt->stride * dt->bytePerPixel, dt->texWidth * dt->bytePerPixel);
		}

		return dt->data;
	}

	return (char *)dt->pixels;
}

bool releaseBufferByTexId(int texId) {
	DirectTexture *dt =  getDirectTexture(texId);

	if (dt == nullptr) {
		ALOGE("Invalid texture id:%d\n", texId);
		return false;
	}

	if (!dt->locked) {
		return true;
	}

	dt->texBuffer->unlock();

	dt->pixels = NULL;
	dt->locked = false;

	return true;
}
