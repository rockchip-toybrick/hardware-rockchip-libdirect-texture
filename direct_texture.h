/*
*    Create By randall.zhuo@rock-chips.com
*    2018/11/01
*
*    OpenGL 2D texture helper.
*     
*    see also rk_ssd_demo
*
*/

#ifndef MY_DIRECT_TEXTURE_H
#define MY_DIRECT_TEXTURE_H

#ifdef __cplusplus 
extern "C"{
#endif
/*
 * Desc:
 *    Create a direct 2D texture which use eglCreateImageKHR and GraphicBuffer.
 *
 * Param:
 *    texWidth:  texture width 
 *    texHeight: texture height
 *    format: GL color format. Only support GL_RGB GL_RGBA now.
 *
 *  Return:
 *    >=0:  texture id
 *    -1: unsupport format 
 *    -2: EGL_NO_DISPLAY
 *    -3: EGL_NO_IMAGE_KHR
 * */

int createDirectTexture(int texWidth, int texHeight, int format);

/*
 * Desc:
 *    Delete the texture created by 'createDirectTexture'
 *
 * Param:
 *	  texId:  texture id
 * */
bool deleteDirectTexture(int texId);

/*
  	Desc:
		Get the buffer of texture, you must call releaseBufferByTexId() when you do not need it.

   	Param:
		texId:  the texture id from createDirectTexture()

*/
char* requireBufferByTexId(int texId);

/*
	Desc:
		release texture buffer. After you call requireBufferByTexId()

   	Param:
		texId:  the texture id from createDirectTexture()
*/
bool releaseBufferByTexId(int texId);


#ifdef __cplusplus 
}
#endif

#endif
