LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := libDirectTexture

LOCAL_CFLAGS += -DGL_GLEXT_PROTOTYPES -DEGL_EGLEXT_PROTOTYPES -Wall -Wextra -Werror

LOCAL_SRC_FILES := \
	    direct_texture.cpp

LOCAL_C_INCLUDES := \

LOCAL_SHARED_LIBRARIES := \
	libEGL	\
	libGLESv2	\
	libui	\
    liblog \
    libutils

LOCAL_PROPRIETARY_MODULE := true

include $(BUILD_SHARED_LIBRARY)
